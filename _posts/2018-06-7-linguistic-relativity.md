---
layout: default
title: "Language of thought"
tags: theories-ideas-and-hypothesis 
---

## What language do we think in?

We think hence we are conscious beings. We spend an overwheming amount of time thinking about work, food, objects, people, etc etc. The grandour architechture of our brain allows us to even think crtically or produce complex thoughts. But what language do we think in? Does language shape our thoughts or thoughts are abstract in nature which we manifest effectively using language? 

This post will be my attempt to answer these questions with my limited knowledge regarding linguistics and psychology.

## Whorfian  hypothesis,  or  the  Sapir–Whorf hypothesis

In academic circles the above is referred as Linguistic Relativity, the above is still called a hypothesis because its still lies in a grey area (I'll come it ). 

Linguistic Relativity is a school of thought (no pun intended) that commits to language affects its speaker's world view or cognition. It has two versions-

1.The strong version - In this perspective language **determines** thought, and linguistic categories limit and determine cognitive categories. Now to put foreward a popular example, there is a tribe in New Guinea called the Hopi, they don't have any grammatical tense in their native language. So it was thought that they did not have any perspective of time but later people studying the language found that the Hopi people have a different way of expressing past, present and future. Thus not quite acceptable version..

2.The weak version - According to this perspective language **influences** thought, or linguistic categories and usage influence thought and decisions. Let me provide you with a great thought experiment in this regard. 

Paint a picture in your head regarding this "A cop chases a thief", now if you have thought that the cop is chasing the thief from your left to right then you write your native language from left to right. So the way you write or read your native languages helps you to express thoughts or precisely influences your thought.

## Thoughts shaping the language what about that?

Moving to the domain where thoughts shape our language. There are two divisions we can make here too-

1.Universalism-This idea is reciprocal of the strong version meaning it states that 'Thought determines language'. or to put firmly expressions requires cognitive understanding. But this is also partly wrong, there is a language in New Guinea (yet again) which has only two words for colours (one for warm colours and one for cool colours). Thus acording to this theory the people who use such language does not see all shades of colours rather they are only able to make differences between light and dark shades which is not true (biology dictates). Consider our primal ancestors chimpanzees they can make distinctions between every colour in the spectrum thus they have the cognitive ability yet they cannot express, thus here lay the contradiction.

2.Piaget's theory - He came up with the theory of cognitive development with children. He stated that thought **influences** language, which mean that when children starts to think in a certain way then they develop certain way to express their thoughts. For example when they see their parents are not around but still they exist at that point they start to develop words like "gone", "missing". thus here thoughts or precisely cognition influences the language.

## Thoughts and language are seperate.

There is another theory by Vygotsky. He stated that language and thoughts are independent of each other. After a certain period we learn to integrate them with the aid of our experience, and express ourselves.


## Which one is correct, what is my point?

I'm not quite qualified to make a point on such debated topic which have been explored by experts for centuries. There is no perfect theory to answer the question I put forward, this topic has revived back in academics after a certain dormant period, but there is no conclusive answer yet.  

<img src="/bin/1984.jpg" style="float: right;"/>


## 1984

Orwell explores this theme in his novel 1984. The newspeak was introduced by the totalitarian government to subjugate thoughts of people by limitng their means to expression. 

But does removing words "democracy, "freedom" really strips the idea of demorcracy and freedom from ones thoughts? How about from early childhood if a person is subjected to such doctrine? Then?

Is it happening to us right now in such flash floods of information, rather 'misinformation' via social media, ad campaigns,a propaganda of falacy- a false utopia (a well designed dystopia). Was Orwell right?


[Image credit](https://www.redbubble.com/shop/1984+orwell+posters),  [Further readings](https://www.coloradocollege.edu/.../3bb4c139-8a46-4238-a5b9-67bca5209ae6.pdf)
