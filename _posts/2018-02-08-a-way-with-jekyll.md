---
layout: default
title: "A way with jekyll"
tags: internet
---



<img src="/bin/jekyll-logo-dark-transparent.png" style="width: 200px; float: right;"/>

## What is jekyll?


In simple words jekyll is static site generator, mainly used for personal, project or organization sites.
It was written by Tom Preston-Werner, Git-hub's co-owner, distributed under open-source liscence.

License: MIT License

Written in: <a href="https://www.ruby-lang.org/en/">Ruby</a>


## Beauty of Open-source code and a free hosting service..


As jekyll takes your Markdown, Liquid,HTML,CSS files and spits out *static* well organized sites which already is ready for deployment.

So you don't have to hardcode alot of things just some basic necessary files (your posts, layout..etc) and you develop a site.

**Jekyll is robust**- Jekyll gives you full freedom customize anyfile. You customize the design, the way you want to display the content, you can change and add any css, html, and javascript files, thus the website is limited to your imagination if you know how to code the web. :)

## Free web-hosting @Git-hub

As jekyll was designed to be compatible with the hosting service of git-hub(gh-pages), so no need of paying a single dime to host your site.. now ain't it beautiful.

<img src="/bin/github-logo.png" style="width: 200px;"/>

## Sharing with a humble community

Jekyll community is for everyone as jekyll is open-source you can use content by others like I'm using for making your site.. or you can develop your own templates, custom themes and host your sites or share them over the community for others to use.

>>Requirements 
>>
>>
    GNU/Linux, Unix, or macOS
    Ruby version 2.2.5 or above, including all development headers (ruby installation can be checked by running ruby -v, development headers can be checked on Ubuntu by running apt list --installed  ruby-dev)
    RubyGems (which you can check by running gem -v)
    GCC and Make (in case your system doesn’t have them installed, which you can check by running gcc -v,g++ -v and make -v in your system’s command line interface)
>>



Learn more @ [Jekyll's official Site ](https://https://jekyllrb.com/)

Creator of this beautiful Windows95 theme - [WINDOWS 95](https://h01000110.github.io/20170917/windows-95)