---
layout: default
title: "Catcher in the rye many-a-times"
tag: books
---

## Journal entry somehwere in Calcutta

The school really sucks, everyone is fucking phoney. Damn I know now how Holden felt even after 63 years I can still relate to him.

I remember asking for a copy from my friend, he said it isn't available in the library for all the cursing and foul language, on that context I had grown an interest to read it, thus saved my lunch money and bought a paperback. Now after a month of major over-haul in my life, I feel so close to Holden, I wish I also could run away from my stupid school, home. Everything sucks,and if I do I don't even have a Phoebe to miss.

## Journal entry somewhere in Calcutta 5 years later

Damn meeting Ankush was so dope. His understanding of western philosophy is so awesome. Having a conversation on impact of existentialism on a post modernist society over some chilled beer, an evening spent well.

His advise was to read Kierkegaard and start from the basics and gradually make my way through Satre, Dostoyevsky and finally Nietzsche.

I returned pretty late, I din't feel like doing anything 'philosophy' still lingered in the air around me. Casually I picked up my old Catcher in the rye, damn the pages have become more yellow. 

Skimming through a few chapters glancing over some paragraphs I had marked, I felt wierd that at somepoint in time this used to be my favourite book. how could I have liked something so juvenile. Holden is a stupid kid who doesn't understand shit. He is fucking afraid of the real world, that old bastard.Out there is so much and he is always whining. This book is so terrible, it has absolutely no depth. So wierd that I liked it so much ..

## Journal entry (turning 30) somewhere in some state of India

Ok I have saved enough, now is the time for a break. When did life become so mundane? I din't get a clue ..

It sorta happened..

Anyway I'm handing this application I need a fucking break, even if it costs me this stupid job. Maybe I can do better, work for a real cause maybe I feel there so much of me still out there to be discovered.

Yesterday I cracked open good old Catcher in the rye, after all these years I still find some sort of closure while reading this book. There's a distance between me and Holden a distance I can never trace but I feel I can always reach him, I am still walking those streets of New York. I remember I used to hate this book in college .. 

Hills would be good for me I guess..


## Journal entry (at 45) 

Its been more than 7 years I've been to Calcutta, I miss that city so much these days. Can't believe Asish is going to 5th grade tomorrow. Another new year, another story.

Yeasterday while cleaning the closet, I found my old Catcher in the rye. The condition is 'Critical' as any medical personnel would call. I flicked some pages sat on the couch before I knew I missed an hour on the clock. 

Now while writing I realise how this book changed my life, back in those days I was so confused .. and even now when the pages are coming out, there is so much to explore through Holden's eyes. 

As a guide, as a son, as father .. as me, he is still out there. And we keep on walking those lonely streets of New york.

<img src="/bin/rye.jpg"  style="width: 500px; display: block; margin-left: auto; margin-right: auto">

*This is a work of fiction, I'm only 20 so not at all based on true events*
